package tasktracker;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TaskTacker {

    public static void main(String[] args) {
        SpringApplication.run(TaskTacker.class, args);
    }

}