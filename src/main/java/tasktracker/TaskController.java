package tasktracker;

import org.springframework.context.annotation.Description;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/task")
public class TaskController {

    //TODO Добавить метод, который вызывается при запросе /{taskId} и возвращает объект Task с помощью метода fakeTask()

    private Task fakeTask(){
        Task t = new Task();
        t.setId(1L);
        t.setName("Create basic controller");
        t.setDescription("Create a separate module 'controllers' and then do some things");
        return t;
    }
    @RequestMapping(method = RequestMethod.GET, value = "/{taskId}")
    public  Task getTaskById(@PathVariable Long taskId ) {
        return fakeTask();
    }


}
